import java.util.Date;

public class Mobile extends Bill implements IDisplay {
	String Mobile_Manufacturer_Name="", Plan_Name="";
	long mobile_number=0;
	double Internet_GB_used=0, Minute_used=0;
	public Mobile(int Bill_id,String Bill_Date,String Bill_Type,double Total_Bill_Amount,String Mobile_Manufacturer_Name, String Plan_Name,long mobile_number,double Internet_GB_used,double Minutes_used) {
		super(Bill_id, Bill_Date, Plan_Name, Minutes_used);
		this.Mobile_Manufacturer_Name=Mobile_Manufacturer_Name;
		this.Plan_Name=Plan_Name;
		this.mobile_number=mobile_number;
		this.Internet_GB_used=Internet_GB_used;
		this.Minute_used=Minutes_used;
		
		// TODO Auto-generated constructor stub
	}
	public String getMobile_Manufacturer_Name() {
		return Mobile_Manufacturer_Name;
	}
	public void setMobile_Manufacturer_Name(String mobile_Manufacturer_Name) {
		Mobile_Manufacturer_Name = mobile_Manufacturer_Name;
	}
	public String getPlan_Name() {
		return Plan_Name;
	}
	public void setPlan_Name(String plan_Name) {
		Plan_Name = plan_Name;
	}
	public long getMobile_number() {
		return mobile_number;
	}
	public void setMobile_number(long mobile_number) {
		this.mobile_number = mobile_number;
	}
	public double getInternet_GB_used() {
		return Internet_GB_used;
	}
	public void setInternet_GB_used(double internet_GB_used) {
		Internet_GB_used = internet_GB_used;
	}
	public double getMinute_used() {
		return Minute_used;
	}
	public void setMinute_used(double minute_used) {
		Minute_used = minute_used;
	}
	@Override
	public void display() {
		super.display();

		System.out.println("\tManufacturer Name: " + Mobile_Manufacturer_Name);
		System.out.println("\tPlan Name : " + Plan_Name);
		System.out.println("\tMobile Number : +1" + mobile_number);
		System.out.println("\tInternet Usage : " + Internet_GB_used + " GB");
		System.out.println("\tMinutes Usage : " +  Minute_used +" minutes");

		System.out.println("\t****************************************************");

	
}
}
