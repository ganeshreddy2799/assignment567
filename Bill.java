
public class Bill implements IDisplay  {
	int	Bill_id=0;
	String Bill_Date ; 
	String Bill_Type=""; 
	Double Total_Bill_Amount=0.0;
	public Bill(int Bill_id,String Bill_Date,String Bill_Type,double Total_Bill_Amount)
	{
		this.Bill_id=Bill_id;
		this.Bill_Type=Bill_Type;
		this.Bill_Date=Bill_Date;
		this.Total_Bill_Amount=Total_Bill_Amount;
	}
	public int getBill_id() {
		return Bill_id;
	}
	public void setBill_id(int bill_id) {
		Bill_id = bill_id;
	}
	public String getBill_Date() {
		return Bill_Date;
	}
	public void setBill_Date(String bill_Date) {
		Bill_Date = bill_Date;
	}
	public String getBill_Type() {
		return Bill_Type;
	}
	public void setBill_Type(String bill_Type) {
		Bill_Type = bill_Type;
	}
	public Double getTotal_Bill_Amount() {
		return Total_Bill_Amount;
	}
	public void setTotal_Bill_Amount(Double total_Bill_Amount) {
		Total_Bill_Amount = total_Bill_Amount;
	}
	@Override
	public void display() {
		// TODO Auto-generated method stub
		System.out.println("The Bill id is "+ Bill_id );
		System.out.println("The Bill date is "+Bill_Date);
		System.out.println("The Bill type is "+Bill_Type);
		System.out.println("The total bill amount is "+Total_Bill_Amount);
	}
	}
