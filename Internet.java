import java.util.Date;

public class Internet extends Bill implements IDisplay {

public Internet(int Bill_id,String Bill_Date,String Bill_Type,double Total_Bill_Amount,String Provider_name,double Internet_GB_Used) {
	super(Bill_id, Bill_Date, Provider_name, Internet_GB_Used);
		this.Provider_name = Provider_name;
		this.Internet_GB_Used=Internet_GB_Used;
		// TODO Auto-generated constructor stub
	}
String Provider_name="";
double Internet_GB_Used=0.0;

public String getProvider_name() {
	return Provider_name;
}
public void setProvider_name(String provider_name) {
	Provider_name = provider_name;
}
public double getInternet_GB_Used() {
	return Internet_GB_Used;
}
public void setInternet_GB_Used(double internet_GB_Used) {
	Internet_GB_Used = internet_GB_Used;
}
public void display() {
	super.display();

	System.out.println("\tProvider Name: " + Provider_name);
	System.out.println("\tInternet Usage: " + Internet_GB_Used + " GB");

	System.out.println("\t****************************************************");
}
}


