import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CustomerList implements IDisplay {

	private ArrayList<Customer> customers = new ArrayList<>();

	public void addCustomer(Customer c) {
		this.customers.add(c);
	}

	public Customer GetCustomerById(int id) {
		Customer c = null;
		for (Customer customer : customers) {
			if (customer.getCustomer_Id() == id) {
				c = customer;
			}
		}

		if (c == null) {
			System.out.println("No customer with id " + id + " exists.");

		}
		return c;
	}

	@Override
	public void display() {
		Collections.sort(customers, new CustomerCompare());
		for (Customer customer : customers) {
			customer.display();
		}
	}

}

class CustomerCompare implements Comparator<Customer> {

	@Override
	public int compare(Customer b1, Customer b2) {
		if (b1.getCustomer_Id() < b2.getCustomer_Id()) {
			return -1;
		} else if (b1.getCustomer_Id() > b2.getCustomer_Id()) {
			return 1;
		} else {
			return 0;
		}
	}

}