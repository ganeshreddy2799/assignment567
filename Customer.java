import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Customer implements IDisplay   {
	
		int Customer_Id=0; 
		String first_name="", last_name="", full_name="",email_id=""; 
		double Total_amount_to_pay=0.0;
		ArrayList <Bill>BillList;
	public Customer(int Customer_Id,String first_name,String last_name,String email_id,double Total_amount_to_pay){
		this.Customer_Id=Customer_Id;
		this.email_id=email_id;
		this.last_name=last_name;
		this.first_name=first_name;
		this.Total_amount_to_pay=Total_amount_to_pay;
		
	}
		public int getCustomer_Id() {
			return Customer_Id;
		}
		public void setCustomer_Id(int customer_Id) {
			Customer_Id = customer_Id;
		}
		public String getFirst_name() {
			return first_name;
		}
		public void setFirst_name(String first_name) {
			this.first_name = first_name;
		}
		public String getLast_name() {
			return last_name;
		}
		public void setLast_name(String last_name) {
			this.last_name = last_name;
		}
		public String getEmail_id() {
			return email_id;
		}
		public void setEmail_id(String email_id) {
			this.email_id = email_id;
		}
		public double getTotal_amount_to_pay() {
			return Total_amount_to_pay;
		}
		public void setTotal_amount_to_pay(double total_amount_to_pay) {
			Total_amount_to_pay = total_amount_to_pay;
		}
		public ArrayList<Bill> getBillList() {
			return BillList;
		}
		public void setBillList(ArrayList<Bill> billList) {
			BillList = billList;
		}
		public String getFullName() {
			return first_name + " " + last_name;
		}
	
public void Billlist(Bill b) {
	BillList.add(b);
}
public double getTotalBillToPay() {
	if (BillList == null || BillList.size() == 0)
		return 0;

	double total = 0;
	for (Bill bill : BillList) {
		total += bill.getTotal_Bill_Amount();
	}
	return total;
}

@Override
public void display() {
	System.out.println("Customer Id: " + Customer_Id);
	System.out.println("Customer Full Name: " + this.getFullName());
	System.out.println("Customer Email Id: " + email_id);

	if (this.getTotalBillToPay() == 0) {
		System.out.println("\t~~~NOTE : This Customer has no bills");
		System.out.println("\t****************************************************");
		return;
	}
	System.out.println("\t             ----Bill Information----               ");
	System.out.println("\t****************************************************");

	Collections.sort(this.BillList, new BillCompare());
	for (Bill bill : this.BillList) {
		bill.display();
	}

	System.out.println("\t    Total Amount to Pay: $" + this.getTotalBillToPay());
	System.out.println("\t****************************************************");

}

}

class BillCompare implements Comparator<Bill> {

@Override
public int compare(Bill b1, Bill b2) {
	if (b1.getBill_id() < b2.getBill_id()) {
		return -1;
	} else if (b1.getBill_id() > b2.getBill_id()) {
		return 1;
	} else {
		return 0;
	}
}

}


	
	
	

	
	
	
 	
	
	 	
